<?php

class LogoAuth extends databaseFactory {
	function __construct() {
		parent::__construct();
	}

	public function authenticateUser($apikey, $hex, $ip) {
		// Check if API key is valid
		$validAPI = $this->checkValidAPI($apikey);
		if(!$validAPI)
			return 1;

		// Check if Hex is valid, and if it is, get users ID
		$validHex = $this->checkValidHex($hex);
		if(!$validHex)
			return 2;

		// Geo locate the IP address
		$userLocation = $this->geolocateIP($ip);
		if(!$userLocation)
			return 3;

		// Geo locate the phone - 1. Send notification to phone, wait up to 10 seconds for new location
		for($i = 0;$i < 10;$i++)
		{
			$phoneLocation = $this->getPhoneLocation($hex);

			// If no location was sent
			if($phoneLocation == 0) {
				sleep(1);
				continue;
			} else {
				break;
			}

		}

		if(!$phoneLocation)
			return 4;

		$distance = $this->calculateDistance($phoneLocation, $userLocation);

		// Compare the locations of the phone and wifi network
		if($distance > 300)
			return 5;

		return 0;
	}

	private function geolocateIP($ip) {
		$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));

		if($query['status'] == "fail")
			return 0;

		$location = array(
			'lat' => $query['lat'],
			'lon' => $query['lon']
		);

		return $location;
	}

	public function calculateDistance($position1, $position2)
	{
		$R = 6378137; // Earth’s mean radius in meter
		$dLat = ($position1['lat'] - $position2['lat']) * pi() / 180;
		$dLon = ($position1['lon'] - $position2['lon']) * pi() / 180;
		$a = sin($dLat / 2) * sin($dLat / 2) + cos(($position1['lat']) * pi() / 180)
				* cos(($position2['lat']) * pi() / 180) * sin($dLon / 2) * sin($dLon / 2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
		$d = $R * $c;
		return $d; // returns the distance in meter
	}
}