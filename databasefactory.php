<?php

class databaseFactory {
	protected $dbAccess;

	function __construct() {
		$dbuser = "root";
		$dbpass = "";
		$db = "logo";
		$dburl = "localhost";

		try {
			$this->dbAccess = new PDO('mysql:host=' . $dburl . ';dbname=' . $db, $dbuser, $dbpass);
			$this->dbAccess->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {
			echo "An error has occured when trying to connect to the database.";
		}
	}

	protected function insertNewUser($deviceid, $hex, $ip) {
		$query = $this->dbAccess->prepare("INSERT INTO accounts (deviceid, hex, num_logins, create_date, last_ip) VALUES (:deviceid, :hex, 0, NOW(), :ip)");
		$query->execute(array(
			':deviceid' => $deviceid,
			':hex' => $hex,
			':ip' => $ip,
		));

		return $query;
	}

	protected function checkAvailableDeviceID($deviceid) {
		$query = $this->dbAccess->prepare("SELECT id FROM accounts WHERE deviceid = :deviceid");
		$query->execute(array(
			':deviceid' => $deviceid,
		));

		if($query->rowCount() == 0)
			return 1;

		return 0;
	}

	protected function checkValidAPI($key) {
		$query = $this->dbAccess->prepare("SELECT id FROM websites WHERE apikey = :key");
		$query->execute(array(
			':key' => $key
		));

		if($query->rowCount() == 1)
			return 1;

		return 0;
	}

	protected function checkValidHex($hex) {
		$query = $this->dbAccess->prepare("SELECT id FROM accounts WHERE hex = :hex");
		$query->execute(array(
			':hex' => $hex
		));

		if($query->rowCount() == 1) {
			$fetchResults = $query->fetch(PDO::FETCH_ASSOC);

			return $fetchResults['id'];
		}

		return 0;
	}

	public function addPhoneLocation($hex, $lat, $lon) {
		// Check valid hex
		if(!$this->checkValidHex($hex))
			return 0;

		$query = $this->dbAccess->prepare("INSERT INTO sentlocations (hex, lat, lon, timestamp, active) VALUES (:hex, :lat, :lon, NOW(), 1)");
		$query->execute(array(
			':hex' => $hex,
			':lat' => $lat,
			':lon' => $lon
		));

		return 1;
	}

	protected function getPhoneLocation($hex) {
		$query = $this->dbAccess->prepare("SELECT lat, lon, id FROM sentlocations WHERE hex = :hex AND active = 1 AND timestamp > (now() - INTERVAL 10 SECOND)");
		$query->execute(array(
			':hex' => $hex
		));

		// If no location was sent to DB
		if($query->rowCount() == 0) {
			return 0;
		}

		$results = $query->fetch(PDO::FETCH_ASSOC);

		$returnResult = array(
			'lat' => $results['lat'],
			'lon' => $results['lon']
		);

		// Disable all previous locations
		$query = $this->dbAccess->prepare("UPDATE sentlocations SET active = 0 WHERE hex = :hex");
		$query->execute(array(
			':hex' => $hex
		));

		return $returnResult;

	}
}