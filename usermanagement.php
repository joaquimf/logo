<?php

class userManagement extends databaseFactory {
	function __construct() {
		parent::__construct();
	}

	public function registerUser($deviceID) {
		$isAvailable = $this->checkAvailableDeviceID($deviceID);

		if(!$isAvailable) {
			return 0;
		}

		$userHex = $this->createHex();
		$ip = $_SERVER['REMOTE_ADDR'];

		$registered = $this->insertNewUser($deviceID, $userHex, $ip);

		if(!$registered)
			return 0;

		return $userHex;
	}

	public function createHex() {
		return sprintf('%07X', mt_rand(0, 0xFFFFFFF));
	}
}